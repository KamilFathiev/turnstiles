# Турникеты - тестовое

## Описание

Репозиторий содержит ноутбук с решением тестового задания для участия в проекте ШИФТ компании ЦФТ, а также файлы с данными для обучения и теста модели.

Задача заключалась в предсказании посетителя по тому, на каких турникетах тот отметился и в какой день и время.

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/KamilFathiev/turnstiles.git
git branch -M main
git push -uf origin main
```
